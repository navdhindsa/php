<!doctype html>
<html>
<head>
    <meta charset="utf-8" />
    <title><?php echo $title ?></title>
    <style>
        body{
            padding: 0;
            margin: 0;
            font-family: 'open-sans', sans-serif;
            background: #efefef;
        }
        div{
            box-sizing: border-box;
        }
        ul.nav{
            list-style-type: none;
            padding: 0;
            margin: 0;
            background: #888;
        }
        ul.nav li{
            display: inline-block;
        }
        ul.nav li a{
            display: inline-block;
            line-height: 40px;
            padding: 0 20px;
            margin: 0;
            text-decoration: none;
            color: #fff;
        }
        ul.nav li a:link, ul.nav li a:visited{
            background: #888;
        }
        ul.nav li a:hover, ul.nav li a:active, ul.nav li a.current {
            background: #444;
        }
        .container{
            width: 100%;
            max-width: 960px;
            margin: 0 auto;
            padding: 30px;
            background: #fff;
        }
        #header, #footer{
            height: 100px;
            padding: 30px;
            color:#fff;
            background: #000;
            font-size: 2rem;
        }
        #footer{
            font-size: .8rem;
            text-align: center;
        }
        #featured_image img{
            width: 100%;
            height: auto;
            max-width: 960px;
        }

    </style>


</head>

<body>
<div class="nav">

    <ul class="nav">
        <li><a <? if($slug=='home'): ?>class="current" <?php endif ?> href="/index.php">Home</a></li>
        <li><a <? if($slug=='about'): ?>class="current" <?php endif ?> href="/about.php">About</a></li>
        <li><a <? if($slug=='shop'): ?>class="current" <?php endif ?> href="/shop.php">Shop</a></li>
        <li><a <? if($slug=='support'): ?>class="current" <?php endif ?> href="/support.php">Support</a></li>
        <li><a <? if($slug=='faqs'): ?>class="current" <?php endif ?> href="/faqs.php">FAQs</a></li>
        <li><a <? if($slug=='contact'): ?>class="current" <?php endif ?> href="/contact.php">Contact</a></li>
    </ul>
</div>
<div id="header">
 PHP TEST SITE!
</div>
